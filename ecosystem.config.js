module.exports = {
  apps : [{
    script: 'src/server.js',
    watch: '.'
  },],

  deploy : {
    production : {
      user : 'root',
      host : '192.168.4.26',
      ref  : 'origin/master',
      repo : 'https://gitlab.com/mobisoft-dev/agridash-acteur-rest-api.git',
      path : '/root/pm2Stuff',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
