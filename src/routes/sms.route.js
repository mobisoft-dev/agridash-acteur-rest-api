const express = require('express');
const auth = require('../middleware/auth.middleware');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');
const smsController = require('../controllers/sms.controller');
const router = express.Router();
const { 
    validateSmsParamters, 
    validateSmsDrParameters 
} = require('../middleware/validators/smsValidator.middleware');

/*****************************************************************************************************************
 * [POST] localhost:3000/api/1.21.0/sms/smsDeliveryReceipt => Visualisation du status des demandes d'envoie de sms
 * [POST] localhost:3000/api/1.21.0/sms/sendMessageToAgriculteur => Demande d'envoie de sms aux acteurs
 *****************************************************************************************************************/

router.post('/smsDeliveryReceipt', auth(), validateSmsDrParameters, awaitHandlerFactory(smsController.mySmsDeliveryReceipt));
router.post('/sendMessageToAgriculteur', auth(), validateSmsParamters, awaitHandlerFactory(smsController.sendSmsToAgriculteur));

module.exports = router;