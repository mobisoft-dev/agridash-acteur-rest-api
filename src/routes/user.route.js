const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { 
    createUserSchema, 
    updateUserSchema, 
    validateLogin 
} = require('../middleware/validators/userValidator.middleware');

/*****************************************************************************************************************
* [GET]    localhost:3000/api/1.21.0/users => Récuperationde la liste de tout les utilisateurs
* [GET]    localhost:3000/api/1.21.0/users/id/{idUtilisateur} => Récuperation d'un utilisatueur
* [GET]    localhost:3000/api/1.21.0/users/ki-suis-je => Voir l'utilisateur connecté
* [POST]   localhost:3000/api/1.21.0/users => Creation d'un utilisateur
* [POST]   localhost:3000/api/1.21.0/users/login => Connexion
* [DELETE] localhost:3000/api/1.21.0/users/id/{idUtilisateur} => Suppression d'un utilisateur
* [PATCH]  localhost:3000/api/1.21.0/users/id/{idUtilisateur} => Mise à jour des informations d'un utilisateur
*****************************************************************************************************************/

router.get('/', auth(), awaitHandlerFactory(userController.getAllUsers)); 
router.get('/id/:id', auth(), awaitHandlerFactory(userController.getUserById)); 
router.get('/ki-suis-je', auth(), awaitHandlerFactory(userController.getCurrentUser));
router.post('/', createUserSchema, awaitHandlerFactory(userController.createUser));
router.post('/login', validateLogin, awaitHandlerFactory(userController.userLogin));
router.delete('/id/:id', auth(Role.Admin), awaitHandlerFactory(userController.deleteUser));
router.patch('/id/:id', auth(Role.Admin), updateUserSchema, awaitHandlerFactory(userController.updateUser)); 

module.exports = router;