const { body } = require('express-validator');
const Role = require('../../utils/userRoles.utils');

exports.createUserSchema = [
    body('username')
        .exists()
        .withMessage('Nom d\'utilisateur est requis')
        .isLength({ min: 3 })
        .withMessage('Doit comporter au moins 3 caractères'),
    body('first_name')
        .exists()
        .withMessage('Votre prénom est obligatoire')
        .isAlpha()
        .withMessage('Doit être uniquement des caractères alphabétiques')
        .isLength({ min: 3 })
        .withMessage('Doit comporter au moins 3 caractères'),
    body('last_name')
        .exists()
        .withMessage('Votre nom de famille est requis')
        .isAlpha()
        .withMessage('Doit être uniquement des caractères alphabétiques')
        .isLength({ min: 3 })
        .withMessage('Doit comporter au moins 3 caractères'),
    body('email')
        .exists()
        .withMessage('L\'e-mail est requis')
        .isEmail()
        .withMessage('Doit être un email valide')
        .normalizeEmail(),
    body('role')
        .optional()
        .isIn([Role.Admin, Role.SuperUser])
        .withMessage('Type de rôle invalide'),
    body('password')
        .exists()
        .withMessage('Le Mot de passe requis')
        .notEmpty()
        .isLength({ min: 6 })
        .withMessage('Le mot de passe doit contenir au moins 6 caractères')
        .isLength({ max: 10 })
        .withMessage('Le mot de passe peut contenir au maximum 10 caractères'),
    body('confirm_password')
        .exists()
        .custom((value, { req }) => value === req.body.password)
        .withMessage('le << confirm_password >> champ doit avoir la même valeur que le champ mot de passe')
];

exports.updateUserSchema = [
    body('username')
        .optional()
        .isLength({ min: 3 })
        .withMessage('Doit comporter au moins 3 caractères'),
    body('first_name')
        .optional()
        .isAlpha()
        .withMessage('Doit être uniquement des caractères alphabétiques')
        .isLength({ min: 3 })
        .withMessage('Doit comporter au moins 3 caractères'),
    body('last_name')
        .optional()
        .isAlpha()
        .withMessage('Doit être uniquement des caractères alphabétiques')
        .isLength({ min: 3 })
        .withMessage('Doit comporter au moins 3 caractères'),
    body('email')
        .optional()
        .isEmail()
        .withMessage('Doit être un email valide')
        .normalizeEmail(),
    body('role')
        .optional()
        .isIn([Role.Admin, Role.SuperUser])
        .withMessage('Type de rôle invalide'),
    body('password')
        .optional()
        .notEmpty()
        .isLength({ min: 6 })
        .withMessage('Le mot de passe doit contenir au moins 6 caractères')
        .isLength({ max: 10 })
        .withMessage('Le mot de passe peut contenir au maximum 10 caractères')
        .custom((value, { req }) => !!req.body.confirm_password)
        .withMessage('Veuillez confirmer votre mot de passe'),
    body('confirm_password')
        .optional()
        .custom((value, { req }) => value === req.body.password)
        .withMessage('le << confirm_password >> champ doit avoir la même valeur que le champ mot de passe'),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Veuillez fournir le champ requis pour mettre à jour')
        .custom(value => {
            const updates = Object.keys(value);
            const allowUpdates = ['username', 'password', 'confirm_password', 'email', 'role', 'first_name', 'last_name'];
            return updates.every(update => allowUpdates.includes(update));
        })
        .withMessage('Invalid updates!')
];

exports.validateLogin = [
    body('email')
        .exists()
        .withMessage('L\'e-mail est requis')
        .isEmail()
        .withMessage('Doit être un email valide')
        .normalizeEmail(),
    body('password')
        .exists()
        .withMessage('Mot de passe requis')
        .notEmpty()
        .withMessage('Le mot de passe doit être rempli')
];