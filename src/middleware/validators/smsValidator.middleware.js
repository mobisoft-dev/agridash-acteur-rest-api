const { body } = require('express-validator');
//powerBI
exports.validateSmsParamters = [
    body('message')
    .exists()
    .withMessage('le champ << message >> est requis')
    .notEmpty()
    .isLength({ min: 5 })
    .withMessage('La taille de << message >> doit être min : 5 caractères')
    .isLength({ max: 160 })
    .withMessage("Le nombre de caractère maximum est de 160 caractère")
    .isString()
    .withMessage('La valeur du champ << message >> doit être une chaine de caractère'),

    /* body('regions')
    .exists()
    .withMessage('le champ << regions >> est requis')
    .notEmpty()
    .isArray({ min: 1 }),

    body('departements')
    .exists()
    .withMessage('le champ << departements >> est requis')
    .notEmpty()
    .isArray({ min: 1 }),

    body('sous_prefectures')
    .exists()
    .withMessage('le champ << sous_prefecture >< est requis')
    .isArray({ min: 1 })
    .withMessage(''), */

    body('localites')
    .exists()
    .withMessage("localite est requis")
    .isArray({ min: 1 })
    .withMessage("le champ localite doit être un tableau contenant des chaînes de carectère ex: ['element1', 'element2', '...']"),
    
    body('speculations')
    .exists()
    .isArray({ min: 1 })
    .withMessage("le champ speculation doit être un tableau contenant des chaînes de carectère ex: ['element1', 'element2', '...']")
];

exports.validateSmsDrParameters = [
    body('date_demande')
    .exists()
    .withMessage('Le champ << date_demande >> est requis')
    .notEmpty()
    .isISO8601('yyyy-mm-dd')
    .withMessage('<< date_demande >> doit être une date au format yyyy-mm-dd')
]