const HttpException = require('../utils/HttpException.utils');
const UserModel = require('../models/user.model');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

const auth = (...roles) => {
    return async function (req, res, next) {
        try {
            const authHeader = req.headers.authorization;
            const bearer = 'Bearer ';

            if (!authHeader || !authHeader.startsWith(bearer)) {
                throw new HttpException(401, 'Accès refusé. Aucune information d\'identification n\'été envoyée !');
            }

            const token = authHeader.replace(bearer, '');
            const secretKey = process.env.SECRET_JWT || "";

            // Vérification du Token
            const decoded = jwt.verify(token, secretKey);
            const user = await UserModel.findOne({ id: decoded.user_id });

            if (!user) {
                throw new HttpException(401, 'Echec d\'authentication!');
            }

            // vérifions si l'utilisateur actuel est l'utilisateur propriétaire de du token
            const ownerAuthorized = req.params.id == user.id;

            // si l'utilisateur actuel n'est pas le propriétaire et
            // si le rôle d'utilisateur n'est pas autorisé à effectuer cette action.
            // l'utilisateur obtiendra cette erreur
            if (!ownerAuthorized && roles.length && !roles.includes(user.role)) {
                throw new HttpException(401, 'Non autorisé');
            }

            // si l'utilisateur a les autorisations
            req.currentUser = user;
            next();

        } catch (e) {
            e.status = 401;
            next(e);
        }
    }
}

module.exports = auth;