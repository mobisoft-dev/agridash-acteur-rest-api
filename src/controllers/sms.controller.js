const AgriculteurModel = require('../models/agriculteur.model');
const orangeSms = require('../sms/orangeSms.js'); 
const HttpException = require('../utils/HttpException.utils');
const { validationResult } = require('express-validator');
const dotenv = require('dotenv');

dotenv.config();

/******************************************************************************
 *                              Sms Controller
 ******************************************************************************/
 class SmsController {
    sendSmsToAgriculteur = async (req, res, next) => {
        this.checkValidation(req);
        const orangeArray = ["7", "8", "9"], 
              mtnciArray  = ["4", "5", "6"], 
              moovciArray = ["1", "2", "3"],
              { password, last_name, first_name, role ,email, ...userWithoutParams } = req.currentUser;

        let message = req.body.message,
            agriculteurList = await AgriculteurModel.findByCriteria({
            nom_lieu: "('"+ req.body.localites.join("','") +"')", 
            speculation_principale: "('"+ req.body.speculations.join("','") +"')"
        }), objTest = [{"tel_principal": "0787080666"},{"tel_principal": "02753551"},{"tel_principal": "07361765"}];

        //let objTest = [{"tel_principal": "87080666"},{"tel_principal": "02753551"}];

        if(!agriculteurList){
            throw new HttpException(404, 'Acteur non trouvé!');
        }

        const lastKey = Object.keys(objTest)[Object.keys(objTest).length-1];
        const lastKey1 = Object.keys(agriculteurList)[Object.keys(agriculteurList).length-1];
        var sms_echoue = 0,
            sms_envoye = 0;
        Object.keys(objTest).forEach(function(key) {
            var number = objTest[key].tel_principal;
            if(number.toString().length < 10 && number.toString().length == 8){
                var getFirstChar = number.substring(1,2);
                if(mtnciArray.includes(getFirstChar)){
                    number = "05"+number;
                }else if(moovciArray.includes(getFirstChar)){
                    number = "01"+number;
                }else if(orangeArray.includes(getFirstChar)){
                    number = "07"+number;
                }
            }

            const options = {
                authorization_header:"Basic "+process.env.HEADER_AUTH,
                area_code:"+225",
                sender_number: number,
                sender_phone: "",
                sms_body: message
            };
            
            if(number.length == 10){
                orangeSms(options).then((responseOrangeSms)=>{
                    console.log(responseOrangeSms.message);
                    responseOrangeSms.message == 201 ? sms_envoye++ : sms_echoue++;
                    if(lastKey == key){
                        const saveResult = AgriculteurModel.saveSmsOutput(
                            {
                                id_user: userWithoutParams.id, 
                                message: message, 
                                nombre_acteur: parseInt(lastKey)+1, 
                                sms_envoye: sms_envoye,
                                sms_echoue: sms_echoue
                            });
                        if(!saveResult){
                            throw new HttpException(500, 'Something went wrong');
                        }
                    }
                }).catch((error)=>{
                    console.log(error.message);
                });

                
            }else{
                console.log('numero acteur inférieur à 10 caractères! : ' + number);
            }
        });

        res.send('Vôtre demande à été prise en compte');
    }

    mySmsDeliveryReceipt = async (req, res, next) => {
        this.checkValidation(req);
        let userSmsDr = await AgriculteurModel.findSmsDr({id_user: req.currentUser.id, date: req.body.date_demande});

        if (!userSmsDr.length) {
            throw new HttpException(404, 'fdfff');
        }

        res.send(userSmsDr);
    }

    checkValidation = (req) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            throw new HttpException(400, 'La validation des informations a echoué!', errors);
        }
    }
 }
 /******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new SmsController;