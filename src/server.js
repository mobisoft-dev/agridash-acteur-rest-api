const express   = require("express");
const dotenv    = require('dotenv');
const cors      = require("cors");
const swaggerUi = require('swagger-ui-express');

const HttpException   = require('./utils/HttpException.utils');
const errorMiddleware = require('./middleware/error.middleware');

const userRouter = require('./routes/user.route');
const smsRouter  = require('./routes/sms.route');

const app = express();
dotenv.config();
app.use(express.json());
app.use(cors());
app.options("*", cors());

const port = Number(process.env.PORT || 3331);

app.use(`/api/1.21.0/users`, userRouter);
app.use(`/api/1.21.0/sms`, smsRouter);
// Erreur 404
app.all('*', (req, res, next) => {
    const err = new HttpException(404, 'Endpoint Not Found');
    next(err);
});


// Erreur middleware
app.use(errorMiddleware);

// starting the server
app.listen(port, () =>
    console.log(`🚀 Server running on port ${port}!`));


module.exports = app;
