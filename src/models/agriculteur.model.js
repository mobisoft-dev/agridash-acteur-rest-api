const query = require('../db/db-connection');
const { multipleColumnSet } = require('../utils/common.utils');

class AgriculteurModel {
    tableName = 'agriculteur';
    logTableName = 'api_call_log';

    find = async (params = {}) => {
        let sql = `SELECT * FROM ${this.tableName}`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        const { columnSet, values } = multipleColumnSet(params)
        sql += ` WHERE ${columnSet}`;

        return await query(sql, [...values]);
    }

    findByCriteria = async (params) => {
        const sql = "SELECT tel_principal FROM "+ 
                    this.tableName 
                    +" WHERE nom_lieu IN "+ 
                    params.nom_lieu 
                    +" AND speculation_principale IN "+
                    params.speculation_principale,
              result = await query(sql);
              
        return result;
    }

    saveSmsOutput = async ({ id_user, message, nombre_acteur , sms_envoye, sms_echoue }) => {
        const sql = `INSERT INTO ${this.logTableName} (id_user, message, nombre_acteur, sms_envoye, sms_echoue) VALUES (?,?,?,?,?)`,
              result = await query(sql, [id_user, message, nombre_acteur, sms_envoye, sms_echoue]),
              affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }

    findSmsDr =  async ({ id_user, date }) => {
        const sql = `SELECT * FROM ${this.logTableName} WHERE id_user = ${id_user} and DATE(date) = '${date}' ORDER BY id DESC`,
              result = await query(sql);

        return result;
    }
}

module.exports = new AgriculteurModel;