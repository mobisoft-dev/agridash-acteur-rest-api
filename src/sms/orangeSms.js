module.exports = (dataObject)=>{
 return new Promise((resolve)=>{
   if(typeof(dataObject) === "object" && dataObject.constructor === Object){
     const dataObjectKeys = Object.keys(dataObject);
     if(dataObjectKeys.length !== 5){
       resolve({message:"You have to provide all the keys of the object"});
     }else{
       if(dataObjectKeys.includes('authorization_header') === true && 
          dataObjectKeys.includes('area_code') === true || 
          dataObjectKeys.includes('receiver_number') === true || 
          dataObjectKeys.includes('sender_phone') === true || 
          dataObjectKeys.includes('sms_body') === true)
        {
         const dataObjectValues = Object.values(dataObject);

       	 if(dataObjectValues.includes(null) === true || 
            dataObjectValues.includes(undefined) === true)
          {
         	resolve({message:"the object key must not have a value like null or undefined"});
       	 }else{
       	 	require("./orangeSmsApi.js")(dataObject)
       	 	.then((responseOrangeSmsApi)=>{
              resolve(responseOrangeSmsApi);
       	 	});
         }
       }else{
         resolve({message:"One or more object keys are incorrectly written"});
       }
     }
   }else{
   	 resolve({message:"The parameter must be an object"});
   }
 });
};